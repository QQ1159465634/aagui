在cmd控制台中

使用方法示例：
img2LCD.exe D:/img.png -mono1  [单色位图 1位]
img2LCD.exe D:/img.png -gray8  [灰度位图 8位]
img2LCD.exe D:/img.png -rgb565 [彩色位图16位]
img2LCD.exe D:/img.png -rgb888 [彩色位图24位]
img2LCD.exe D:/img.png -bgr888 [彩色位图24位]
img2LCD.exe D:/img.png -rgba   [彩色位图32位]
img2LCD.exe D:/img.png -bgra   [彩色位图32位]
img2LCD.exe D:/img.png -qoi    [彩色压缩图片]
默认生成C文件，可选-bin生成二进制文件，生成的文件在原图片文件夹